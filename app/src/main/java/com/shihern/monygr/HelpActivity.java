package com.shihern.monygr;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class HelpActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    Button next;
    Button previous;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.help_toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.help_container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        next = (Button)findViewById(R.id.help_next_button);
        previous = (Button)findViewById(R.id.help_previous_button);

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = mViewPager.getCurrentItem() + 1;
                mViewPager.setCurrentItem(position);
            }
        });

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = mViewPager.getCurrentItem() - 1;
                mViewPager.setCurrentItem(position);
            }
        });

    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";
        TextView title;
        TextView description;
        ImageView image;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_help, container, false);
            title = (TextView) rootView.findViewById(R.id.help_title);
            description = (TextView) rootView.findViewById(R.id.help_description);
            image = (ImageView) rootView.findViewById(R.id.help_picture);

            DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
            int height = (int)(displayMetrics.heightPixels / displayMetrics.density * 0.75);
            int width = (int)(displayMetrics.widthPixels / displayMetrics.density * 0.75);

            switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 1:
                    title.setText(getResources().getString(R.string.help_title_1));
                    description.setText(getResources().getString(R.string.help_description_1));
                    image.setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.screenshot_home, width, height));
                    break;
                case 2:
                    title.setText(getResources().getString(R.string.help_title_2));
                    description.setText(getResources().getString(R.string.help_description_2));
                    image.setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.screenshot_navigation, width, height));
                    break;
                case 3:
                    title.setText(getResources().getString(R.string.help_title_3));
                    description.setText(getResources().getString(R.string.help_description_3));
                    image.setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.screenshot_createtransaction, width, height));
                    break;
                case 4:
                    title.setText(getResources().getString(R.string.help_title_4));
                    description.setText(getResources().getString(R.string.help_description_4));
                    image.setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.screenshot_transactionslist, width, height));
                    break;
                case 5:
                    title.setText(getResources().getString(R.string.help_title_5));
                    description.setText(getResources().getString(R.string.help_description_5));
                    image.setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.screenshot_transactiondetails, width, height));
                    break;
                case 6:
                    title.setText(getResources().getString(R.string.help_title_6));
                    description.setText(getResources().getString(R.string.help_description_6));
                    image.setImageBitmap(decodeSampledBitmapFromResource(getResources(), R.drawable.screenshot_budget, width, height));
                    break;
                case 7:
                    title.setText(getResources().getString(R.string.help_title_7));
                    description.setText(getResources().getString(R.string.help_description_7));
                    image.setVisibility(View.GONE);
            }

            return rootView;
        }

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 7;
        }
    }
}
