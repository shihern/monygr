package com.shihern.monygr;


import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.NumberFormat;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private PieChart pieChart;
    private TextView income;
    private TextView expenditure;
    NumberFormat fmt;
    SQLController sqlcon;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sqlcon = SQLController.open(getContext());
        fmt = NumberFormat.getCurrencyInstance();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Monygr");
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        income = (TextView)v.findViewById(R.id.home_income);
        expenditure = (TextView)v.findViewById(R.id.home_expenditure);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        pieChart = (PieChart)getActivity().findViewById(R.id.piechart);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setHoleRadius(40f);
        pieChart.setTransparentCircleRadius(45f);
        pieChart.getLegend().setEnabled(true);
        pieChart.getLegend().setTextSize(12f);
        pieChart.getLegend().setWordWrapEnabled(true);
        pieChart.setDrawSliceText(false);
        //pieChart.setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        pieChart = null;
        income = null;
        expenditure = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        setData();
        pieChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setImageResource(R.drawable.ic_add_24dp);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CreateTransactionActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setData() {

        ArrayList<Entry> yVals1 = new ArrayList<Entry>(); //arraylist of amount
        ArrayList<String> xVals = new ArrayList<String>(); //arraylist of category

        Cursor cursor = sqlcon.getAmountSumByCategory();

        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.
        int id = 0;
        while(!cursor.isAfterLast()) {
            yVals1.add(new Entry((float)cursor.getDouble(1), id));
            xVals.add(cursor.getString(0) + " - " + fmt.format(cursor.getDouble(1)));
            id++;
            cursor.moveToNext();
        }

        PieDataSet dataSet = new PieDataSet(yVals1, "");
        dataSet.setSliceSpace(2f);
        dataSet.setSelectionShift(7f);

        // add a lot of colors
        ArrayList<Integer> colors = new ArrayList<Integer>();
        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);
        colors.add(ColorTemplate.getHoloBlue());
        dataSet.setColors(colors);

        PieData data = new PieData(xVals, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(12f);
        data.setValueTextColor(Color.BLACK);
        pieChart.setData(data);
        pieChart.setUsePercentValues(true);

        income.setText("+" + fmt.format(sqlcon.getTotalIncome()));
        expenditure.setText("-" + fmt.format(sqlcon.getTotalExpenditure()));

        // undo all highlights
        pieChart.highlightValues(null);
        pieChart.setDescription("");

        pieChart.invalidate();
    }

}
