package com.shihern.monygr;

import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 */
public class TransactionContent {

    /**
     * An array of sample (dummy) items.
     */
    public static List<Transaction> ITEMS = new ArrayList<Transaction>();

    public static void addItem(Transaction item) {
        ITEMS.add(item);
        sortListByDate();
    }

    public static void removeItem(Transaction item) {
        ITEMS.remove(item);
        sortListByDate();
    }

//    public static void editItem(Transaction item) {
//        int index = ITEMS.indexOf(item);
//        if(index != -1) {
//
//        }
//
//        for(Transaction t: ITEMS) {
//            if(t.rowId == item.rowId) {
//                t.title = item.title;
//                t.description = item.description;
//                t.category = item.category;
//                t.amount = item.amount;
//                t.date = item.date;
//                t.isWithdrawal = item.isWithdrawal;
//                sortListByDate();
//                break;
//            }
//        }
//    }

    public static void reinitializeDataFromDB(Context context) {
        SQLController sqlcon = SQLController.open(context);
        Cursor c = sqlcon.getTransactions();
        ITEMS = new ArrayList<Transaction>();
        while(!c.isAfterLast()) {
            Transaction t = new Transaction();
            t.rowId = c.getLong(0);
            t.title = c.getString(1);
            t.description = c.getString(2);
            t.category = c.getString(3);
            t.amount = c.getDouble(4);
            t.date = c.getLong(5);
            t.isWithdrawal = c.getInt(6)==1;
            addItem(t);
            c.moveToNext();
        }

        sortListByDate();
    }

    public static void sortListByDate() {
        Collections.sort(ITEMS);
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class Transaction implements Comparable<Transaction>, Parcelable{
        public long rowId = -1;
        public String title;
        public String description;
        public String category;
        public double amount;
        public long date;
        public Boolean isWithdrawal;

        public Transaction() {}

        /**
         * @param title The transaction title
         * @param description The transaction description
         * @param category Category of the transaction
         * @param amount The transaction value (values must be positive)
         * @param date The date of the transaction (in Epoch milliseconds)
         * @param isWithdrawal Whether the transaction was a withdrawing money (true), or depositing money (false)
         */
        public Transaction(String title, String description, String category, double amount, long date, Boolean isWithdrawal) {
            this.title = title;
            this.description = description;
            this.category = category;
            this.amount = amount;
            this.date = date;
            this.isWithdrawal = isWithdrawal;
        }

        @Override
        public String toString() {
            return title;
        }

        public int compareTo(Transaction other) {
            if(this.date < other.date)
                return 1;
            else if (this.date > other.date)
                return -1;
            else
                return 0;
        }

        private Transaction(long id, Parcel in) {
            rowId = id;
            title = in.readString();
            description = in.readString();
            category = in.readString();
            amount = in.readDouble();
            date = in.readLong();
            byte isWithdrawalVal = in.readByte();
            isWithdrawal = isWithdrawalVal == 0x02 ? null : isWithdrawalVal != 0x00;
        }

        /**
         * Returns a {@link Transaction} object. The returned object will be an existing
         * {@code Transaction} in {@link TransactionContent#ITEMS} if it exists, or a new
         * {@code Transaction} otherwise.
         *
         * @param in The parcel containing the data for a {@code transaction}
         * @return The {@code Transaction} object specified by the {@code Parcel}
         */
        protected static Transaction getTransaction(Parcel in) {
            long rowId = in.readLong();
            for(Transaction t: ITEMS) {
                if(t.rowId == rowId) {
                    return t;
                }
            }
            return new Transaction(rowId, in);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeLong(rowId);
            dest.writeString(title);
            dest.writeString(description);
            dest.writeString(category);
            dest.writeDouble(amount);
            dest.writeLong(date);
            if (isWithdrawal == null) {
                dest.writeByte((byte) (0x02));
            } else {
                dest.writeByte((byte) (isWithdrawal ? 0x01 : 0x00));
            }
        }

        @SuppressWarnings("unused")
        public static final Parcelable.Creator<Transaction> CREATOR = new Parcelable.Creator<Transaction>() {
            @Override
            public Transaction createFromParcel(Parcel in) {
                return getTransaction(in);
            }

            @Override
            public Transaction[] newArray(int size) {
                return new Transaction[size];
            }
        };

    }
}
