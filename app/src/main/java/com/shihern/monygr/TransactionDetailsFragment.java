package com.shihern.monygr;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.shihern.monygr.TransactionContent.Transaction;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 */
public class TransactionDetailsFragment extends Fragment {
    public static final int EDIT_RESULT = 100;

    private Transaction transaction;

    private TextView title_view;
    private TextView description_view;
    private TextView category_view;
    private TextView date_view;
    private TextView amount_view;

    private String titleTransitionName;
    private String descriptionTransitionName;
    private String categoryTransitionName;
    private String amountTransitionName;
    private String dateTransitionName;

    public TransactionDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //save the transition names and transaction objects into variables
        if (getArguments() != null) {
            titleTransitionName = getArguments().getString("trans_title");
            descriptionTransitionName = getArguments().getString("trans_description");
            categoryTransitionName = getArguments().getString("trans_category");
            amountTransitionName = getArguments().getString("trans_amount");
            dateTransitionName = getArguments().getString("trans_date");

            transaction = getArguments().getParcelable("transaction");
        }
        //set the transaction correctly is thsi fragment is recreated (e.g. screen rotated)
        if(savedInstanceState != null && savedInstanceState.getParcelable("transaction") != null) {
            transaction = savedInstanceState.getParcelable("transaction");
        }
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transaction_details, container, false);
        title_view = (TextView)view.findViewById(R.id.transaction_details_title);
        description_view = (TextView)view.findViewById(R.id.transaction_details_description);
        category_view = (TextView)view.findViewById(R.id.transaction_details_category);
        date_view = (TextView)view.findViewById(R.id.transaction_details_date);
        amount_view = (TextView)view.findViewById(R.id.transaction_details_amount);

        updateTransactionVariables();

        //Set the transition names in this fragment for the animation to work
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            title_view.setTransitionName(titleTransitionName);
            description_view.setTransitionName(descriptionTransitionName);
            category_view.setTransitionName(categoryTransitionName);
            amount_view.setTransitionName(amountTransitionName);
            date_view.setTransitionName(dateTransitionName);
        }

        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setImageResource(R.drawable.ic_create_24dp);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), EditTransactionActivity.class);
                intent.putExtra(EditTransactionActivity.TRANSACTION_PARCEL, transaction);
                startActivityForResult(intent, EDIT_RESULT);
            }
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.transaction_details, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_delete) {
            if(SQLController.open(getContext()).removeTransaction(transaction)) {
                TransactionContent.removeItem(transaction);
                transaction = null;
                Toast.makeText(getContext(), "Transaction has been deleted", Toast.LENGTH_SHORT).show();
                getFragmentManager().popBackStackImmediate();
            } else {
                Toast.makeText(getContext(), "Error deleting transaction", Toast.LENGTH_SHORT).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == EDIT_RESULT) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                transaction = data.getParcelableExtra("transaction");
                updateTransactionVariables();
            }
        }
    }

    private void updateTransactionVariables() {
        try {
            Date d = new Date(transaction.date);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // the format of your date
            sdf.setTimeZone(TimeZone.getDefault());
            String date = sdf.format(d);

            NumberFormat fmt = NumberFormat.getCurrencyInstance();
            String amount = fmt.format(transaction.amount);

            title_view.setText(transaction.title);
            description_view.setText(transaction.description);
            category_view.setText(transaction.category);
            date_view.setText(date);
            amount_view.setText(amount);
        } catch (NullPointerException e) {
            Toast.makeText(getContext(), "Error retrieving data", Toast.LENGTH_SHORT).show();
            getFragmentManager().popBackStackImmediate();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.
        savedInstanceState.putParcelable("transaction", transaction);
        super.onSaveInstanceState(savedInstanceState);
    }

}
