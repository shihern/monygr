package com.shihern.monygr;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by shihern on 22/3/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper{
    // MODULES TABLE INFORMATTION
    public static final String TRANS_TABLE = "transactionsTable";
    public static final String TRANS_ID = "_id";
    public static final String TRANS_TITLE = "title";
    public static final String TRANS_DESCRIPTION = "description";
    public static final String TRANS_CATEGORY = "category";
    public static final String TRANS_AMOUNT = "amount";
    public static final String TRANS_ISWITHDRAWAL = "isWithdrawal";
    public static final String TRANS_DATE = "date";

    // DATABASE INFORMATION
    static final String DB_NAME = "TRANSACTIONS.DB";
    static final int DB_VERSION = 3;

    // TABLE CREATION STATEMENT
    private static final String CREATE_MODULES_TABLE = "create table " + TRANS_TABLE + "("
            + TRANS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + TRANS_TITLE + " TEXT NOT NULL , "
            + TRANS_DESCRIPTION + " TEXT NOT NULL ,"
            + TRANS_CATEGORY + " TEXT NOT NULL ,"
            + TRANS_AMOUNT + " DOUBLE NOT NULL ,"
            + TRANS_DATE + " INTEGER NOT NULL ,"
            + TRANS_ISWITHDRAWAL + " INTEGER NOT NULL );";

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_MODULES_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TRANS_TABLE);
        onCreate(db);

    }
}
