package com.shihern.monygr;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.shihern.monygr.TransactionContent.Transaction;

import java.util.Calendar;

/**
 * Created by shihern on 10/4/2016.
 */
public class EditTransactionActivity extends CreateTransactionActivity{

    public final static String TRANSACTION_PARCEL = "transaction";
    Transaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setTitle("Edit Transaction");
        transaction = getIntent().getParcelableExtra(TRANSACTION_PARCEL);
    }

    @Override
    public void onStart() {
        super.onStart();
        title_input.setText(transaction.title);
        description_input.setText(transaction.description);
        amount_input.setText(String.valueOf(transaction.amount));

        String[] categories;
        if(transaction.isWithdrawal) { //is an expenditure
            button_expenditure.setChecked(true);
            button_income.setChecked(false);
            //tell the class to update the isWithdrawal variable and category spinner
            super.onRadioButtonClicked(button_expenditure);
            categories = getResources().getStringArray(R.array.categories_expenditure);
        } else { //is an income
            button_expenditure.setChecked(false);
            button_income.setChecked(true);
            //tell the class to update the isWithdrawal variable and category spinner
            super.onRadioButtonClicked(button_income);
            categories = getResources().getStringArray(R.array.categories_income);
        }

        String category = transaction.category;
        for(int i = 0; i < categories.length; i++) {
            if(categories[i].equalsIgnoreCase(category)) {
                category_input.setSelection(i);
                break;
            }
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(transaction.date);
        date_input.updateDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    void createTransaction() {
        if(!isValidInput()) {
            return;
        }
        //category and isWithdrawal is already set
        title = title_input.getText().toString();
        description = description_input.getText().toString();
        amount = Double.parseDouble(amount_input.getText().toString());
        date = getDateFromDatePicker(date_input);

        transaction.title = title;
        transaction.description = description;
        transaction.category = category;
        transaction.amount = amount;
        transaction.date = date;
        transaction.isWithdrawal = isWithdrawal;

        SQLController.open(this).editTransaction(transaction);
        TransactionContent.sortListByDate();

        Intent intent = new Intent();
        intent.putExtra("transaction", transaction);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }
}
