package com.shihern.monygr;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.shihern.monygr.TransactionContent.Transaction;

/**
 * Controller class to manage the SQL Database.
 * All changes to the database should be handled by this class.
 * Use the {@link #open(Context)} method to get a Singleton.
 * All non-static methods should only be accessed through the Singleton.
 * New instances cannot be created by another class.
 *
 * @author shihern
 * @since  2016-03-26
 */
public class SQLController {
    private static SQLController sqlSingleton;
    private Context context;
    private DatabaseHelper dbhelper;
    private SQLiteDatabase database;

    private SQLController(Context c) {
        context = c;
    }

    public static SQLController open(Context context) throws SQLException {
        if(sqlSingleton == null) {
            sqlSingleton = new SQLController(context);
            sqlSingleton.dbhelper = new DatabaseHelper(sqlSingleton.context);
            sqlSingleton.database = sqlSingleton.dbhelper.getWritableDatabase();
        }
        if(!sqlSingleton.database.isOpen()) {
            sqlSingleton.dbhelper = new DatabaseHelper(sqlSingleton.context);
            sqlSingleton.database = sqlSingleton.dbhelper.getWritableDatabase();
        }
        sqlSingleton.context = context;
        return sqlSingleton;
    }

    public void close() {
        dbhelper.close();
    }

    /**
     * Adds a transaction to the local transaction database.
     *
     * @param transaction The {@link Transaction} containing the data to be added
     * @return The row id of the transaction in the database
     */
    public long addTransaction(Transaction transaction) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.TRANS_TITLE, transaction.title);
        cv.put(DatabaseHelper.TRANS_DESCRIPTION, transaction.description);
        cv.put(DatabaseHelper.TRANS_CATEGORY, transaction.category);
        cv.put(DatabaseHelper.TRANS_AMOUNT, transaction.amount);
        cv.put(DatabaseHelper.TRANS_DATE, transaction.date);
        cv.put(DatabaseHelper.TRANS_ISWITHDRAWAL, transaction.isWithdrawal ? 1 : 0);
        return database.insert(DatabaseHelper.TRANS_TABLE, null, cv);
    }

    /**
     * Removes a transaction from the local transaction database.
     *
     * @param transaction The {@link Transaction} to remove from the database
     * @return {@code true} if successful, or {@code false} if an error occurred
     */
    public Boolean removeTransaction(Transaction transaction){
        if(transaction.rowId == -1) {
            return false;
        }
        int noOfRowsDeleted = database.delete(DatabaseHelper.TRANS_TABLE, DatabaseHelper.TRANS_ID + " = " + String.valueOf(transaction.rowId), null);

        return noOfRowsDeleted > 0;
    }

    public void editTransaction(Transaction transaction) {
        ContentValues cv = new ContentValues();
        cv.put(DatabaseHelper.TRANS_TITLE, transaction.title);
        cv.put(DatabaseHelper.TRANS_DESCRIPTION, transaction.description);
        cv.put(DatabaseHelper.TRANS_CATEGORY, transaction.category);
        cv.put(DatabaseHelper.TRANS_AMOUNT, transaction.amount);
        cv.put(DatabaseHelper.TRANS_DATE, transaction.date);
        cv.put(DatabaseHelper.TRANS_ISWITHDRAWAL, transaction.isWithdrawal ? 1 : 0);

        database.update(DatabaseHelper.TRANS_TABLE, cv, DatabaseHelper.TRANS_ID + " = " + String.valueOf(transaction.rowId), null);
    }

    public Cursor getTransactions() {
        String[] allColumns = new String[] {
                DatabaseHelper.TRANS_ID,
                DatabaseHelper.TRANS_TITLE,
                DatabaseHelper.TRANS_DESCRIPTION,
                DatabaseHelper.TRANS_CATEGORY,
                DatabaseHelper.TRANS_AMOUNT,
                DatabaseHelper.TRANS_DATE,
                DatabaseHelper.TRANS_ISWITHDRAWAL
        };

        Cursor c = database.query(DatabaseHelper.TRANS_TABLE, allColumns, null, null, null,
                                  null, null);

        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    /**
     * Retrieves the sum of amounts for each category where is withdrawal is true.
     * The table is formatted as
     *
     * <table style="border: 1px solid gray">
     *     <tr>
     *         <td style="border: 1px solid gray">Category</td>
     *         <td style="border: 1px solid gray">Total Amount</td>
     *     </tr>
     *     <tr>
     *         <td style="border: 1px solid gray">Data</td>
     *         <td style="border: 1px solid gray">Data</td>
     *     </tr>
     * </table>
     *
     * @return The cursor containing the sum of amounts grouped by category
     */
    public Cursor getAmountSumByCategory() {
        /* SELECT CATEGORY,SUM(AMOUNT)
         * FROM TABLE
         * WHERE ISWITHDRAWAL = 1
         * GROUP BY CATEGORY
         * ORDER BY CATEGORY
         */

        String[] columns = new String[] {
                DatabaseHelper.TRANS_CATEGORY,
                "sum(" + DatabaseHelper.TRANS_AMOUNT + ")"
        };
        Cursor c = database.query(DatabaseHelper.TRANS_TABLE, columns, DatabaseHelper.TRANS_ISWITHDRAWAL + " = 1", null, DatabaseHelper.TRANS_CATEGORY, null, DatabaseHelper.TRANS_CATEGORY);

        if (c != null) {
            c.moveToFirst();
        }
        return c;
    }

    public double getTotalExpenditure() {
        String[] columns = new String[] {
                "sum(" + DatabaseHelper.TRANS_AMOUNT + ")"
        };
        Cursor c = database.query(DatabaseHelper.TRANS_TABLE, columns, DatabaseHelper.TRANS_ISWITHDRAWAL + " = 1", null, null, null, null);

        try {
            c.moveToFirst();
            return c.getDouble(0);
        } catch (NullPointerException e) {
            return 0;
        }
    }

    public double getTotalIncome() {
        String[] columns = new String[] {
                "sum(" + DatabaseHelper.TRANS_AMOUNT + ")"
        };
        Cursor c = database.query(DatabaseHelper.TRANS_TABLE, columns, DatabaseHelper.TRANS_ISWITHDRAWAL + " = 0", null, null, null, null);

        try {
            c.moveToFirst();
            return c.getDouble(0);
        } catch (NullPointerException e) {
            return 0;
        }
    }


}
