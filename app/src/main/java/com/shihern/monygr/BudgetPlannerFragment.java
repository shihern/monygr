package com.shihern.monygr;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class BudgetPlannerFragment extends Fragment {
    EditText monthlyIncome;
    EditText desiredSavings;
    EditText averageFood;
    EditText mealsPerDay;
    EditText billsInput;
    EditText housingInput;
    EditText othersInput;


    public BudgetPlannerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_budget_planner, container, false);

        monthlyIncome = (EditText)view.findViewById(R.id.budget_monthly_income);
        desiredSavings = (EditText)view.findViewById(R.id.budget_desired_savings);
        averageFood = (EditText)view.findViewById(R.id.budget_average_food);
        mealsPerDay = (EditText)view.findViewById(R.id.budget_meals_per_day);
        billsInput = (EditText)view.findViewById(R.id.budget_average_bills);
        housingInput = (EditText)view.findViewById(R.id.budget_housing);
        othersInput = (EditText)view.findViewById(R.id.budget_others);

        Button btn = (Button)view.findViewById(R.id.budget_calculate);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValidInput()) {
                    calculate();
                }
            }
        });


        return view;
    }

    private void calculate() {
        double income = Double.parseDouble(monthlyIncome.getText().toString());
        double savings = Double.parseDouble(desiredSavings.getText().toString());
        double food = Double.parseDouble(averageFood.getText().toString());
        int noOfMeals = Integer.parseInt(mealsPerDay.getText().toString());
        double bills = Double.parseDouble(billsInput.getText().toString());
        double housing = Double.parseDouble(housingInput.getText().toString());
        double others = Double.parseDouble(othersInput.getText().toString());

        int iYear = Calendar.getInstance().get(Calendar.YEAR);
        int iMonth = Calendar.getInstance().get(Calendar.MONTH);
        int iDay = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
        // Create a calendar object and set year and month
        Calendar mycal = new GregorianCalendar(iYear, iMonth, iDay);
        // Get the number of days in that month
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH); // 28

        double leftover = income - savings - food*noOfMeals*daysInMonth - bills - housing - others;

        double averageFood;
        double maxFood;
        double totalLeft;

        if(leftover < 0) {
            double leftoverWithoutFood = leftover + food*noOfMeals*daysInMonth;
            averageFood = leftoverWithoutFood/noOfMeals/daysInMonth;
            maxFood = averageFood;
            totalLeft = 0;
        } else if(leftover == 0) {
            averageFood = 0;
            maxFood = 0;
            totalLeft = 0;
        } else {
            averageFood = food;
            maxFood = averageFood + leftover/noOfMeals/daysInMonth;
            totalLeft = leftover;
        }

        getActivity().findViewById(R.id.budget_input).setVisibility(View.GONE);
        getActivity().findViewById(R.id.budget_results).setVisibility(View.VISIBLE);
        TextView averageMeals = (TextView)getActivity().findViewById(R.id.result_average_meals);
        TextView maxMeals = (TextView)getActivity().findViewById(R.id.result_spending_meals);
        TextView spendingBills = (TextView)getActivity().findViewById(R.id.result_spending_bills);
        TextView spendingHousing = (TextView)getActivity().findViewById(R.id.result_spending_housing);
        TextView spendingOthers = (TextView)getActivity().findViewById(R.id.result_spending_others);
        TextView spendingNonEssential = (TextView)getActivity().findViewById(R.id.result_spending_nonessential);

        NumberFormat fmt = NumberFormat.getCurrencyInstance();

        averageMeals.setText(averageMeals.getText() + fmt.format(averageFood));
        maxMeals.setText(maxMeals.getText() + fmt.format(maxFood));
        spendingBills.setText(spendingBills.getText() + fmt.format(bills));
        spendingHousing.setText(spendingHousing.getText() + fmt.format(housing));
        spendingOthers.setText(spendingOthers.getText() + fmt.format(others));
        spendingNonEssential.setText(spendingNonEssential.getText() + fmt.format(totalLeft));

    }

    private Boolean isValidInput() {
        Boolean isValid = true;
        if(TextUtils.isEmpty(monthlyIncome.getText().toString())) {
            isValid = false;
            monthlyIncome.setError("This field is required");
        }
        if(TextUtils.isEmpty(desiredSavings.getText().toString())) {
            isValid = false;
            desiredSavings.setError("This field is required");
        }
        if(TextUtils.isEmpty(averageFood.getText().toString())) {
            isValid = false;
            averageFood.setError("This field is required");
        }
        if(TextUtils.isEmpty(mealsPerDay.getText().toString())) {
            isValid = false;
            mealsPerDay.setError("This field is required");
        }
        if(TextUtils.isEmpty(billsInput.getText().toString())) {
            isValid = false;
            billsInput.setError("This field is required");
        }
        if(TextUtils.isEmpty(housingInput.getText().toString())) {
            isValid = false;
            housingInput.setError("This field is required");
        }
        if(TextUtils.isEmpty(othersInput.getText().toString())) {
            isValid = false;
            othersInput.setError("This field is required");
        }
        if(Double.parseDouble(monthlyIncome.getText().toString()) <= Double.parseDouble(desiredSavings.getText().toString())) {
            isValid = false;
            desiredSavings.setError("Savings must be less than income");
        }

        return isValid;
    }

}
