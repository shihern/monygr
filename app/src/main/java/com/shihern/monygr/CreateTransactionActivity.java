package com.shihern.monygr;


import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.shihern.monygr.TransactionContent.Transaction;

import java.util.Calendar;

public class CreateTransactionActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    EditText title_input;
    EditText description_input;
    EditText amount_input;
    RadioButton button_expenditure;
    RadioButton button_income;
    Spinner category_input;
    DatePicker date_input;
    TextView categoryErrorText;
    TextView transactionType;


    String title;
    String description;
    String category;
    double amount;
    long date; //date in Unix Time
    Boolean isWithdrawal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_transaction);
        Toolbar toolbar = (Toolbar) findViewById(R.id.create_transaction_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Add Transaction");
        ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_close_24dp);
        ab.setDisplayHomeAsUpEnabled(true);

        title_input = (EditText)findViewById(R.id.create_transaction_title);
        description_input = (EditText)findViewById(R.id.create_transaction_description);
        amount_input = (EditText)findViewById(R.id.create_transaction_amount);
        button_expenditure = (RadioButton)findViewById(R.id.button_expenditure);
        button_income = (RadioButton)findViewById(R.id.button_income);
        category_input = (Spinner)findViewById(R.id.create_transaction_category);
        date_input = (DatePicker)findViewById(R.id.create_transaction_date);
        categoryErrorText = (TextView)findViewById(R.id.create_transaction_category_error);
        transactionType = (TextView)findViewById(R.id.create_transaction_type);
        category_input.setEnabled(false);
        category_input.setOnItemSelectedListener(this);

    }

    void createTransaction() {
        if(!isValidInput()) {
            return;
        }
        //category and isWithdrawal is already set
        title = title_input.getText().toString();
        description = description_input.getText().toString();
        amount = Double.parseDouble(amount_input.getText().toString());
        date = getDateFromDatePicker(date_input);

        Transaction transaction = new Transaction(title, description, category, amount, date, isWithdrawal);
        transaction.rowId = SQLController.open(this).addTransaction(transaction);
        TransactionContent.addItem(transaction);

        setResult(Activity.RESULT_OK);
        finish();
    }

    Boolean isValidInput() {
        Boolean isValid = true;
        if(TextUtils.isEmpty(title_input.getText().toString())) {
            isValid = false;
            title_input.setError("This field is required");
        }
        if(TextUtils.isEmpty(description_input.getText().toString())) {
            isValid = false;
            description_input.setError("This field is required");
        }
        if(TextUtils.isEmpty(amount_input.getText().toString())) {
            isValid = false;
            amount_input.setError("This field is required");
        }
        if(isWithdrawal == null) { //this variable is set if a radio button is selected
            isValid = false;
            transactionType.setError("Selecting an option is required");
        }
        if(TextUtils.isEmpty(category)) { //this variable is set if an item in the spinner was selected
            isValid = false;
            categoryErrorText.setVisibility(View.VISIBLE);
        }

        return isValid;
    }

    public static long getDateFromDatePicker(DatePicker datePicker){
        int day = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year =  datePicker.getYear();

        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day);

        return calendar.getTimeInMillis();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_transaction, menu);
        return true;
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.button_expenditure:
                if (checked) {
                    isWithdrawal = true;
                    setSpinnerCategories();
                    transactionType.setError(null);
                }
                break;
            case R.id.button_income:
                if (checked) {
                    isWithdrawal = false;
                    setSpinnerCategories();
                    transactionType.setError(null);
                }
                break;
        }
    }

    private void setSpinnerCategories() {
        if(isWithdrawal == null) {
            return;
        }
        ArrayAdapter<CharSequence> adapter;
        if(isWithdrawal) {
            // Create an ArrayAdapter using the string array and a default spinner layout
            adapter = ArrayAdapter.createFromResource(this,
                R.array.categories_expenditure, android.R.layout.simple_spinner_item);
        } else {
            // Create an ArrayAdapter using the string array and a default spinner layout
            adapter = ArrayAdapter.createFromResource(this,
                R.array.categories_income, android.R.layout.simple_spinner_item);
        }
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        category_input.setAdapter(adapter);
        category_input.setEnabled(true);
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        category = (String)parent.getItemAtPosition(pos);
        categoryErrorText.setVisibility(View.GONE);
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create_transaction_confirm:
                createTransaction();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}
