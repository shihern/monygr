package com.shihern.monygr;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionInflater;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shihern.monygr.TransactionContent.Transaction;


/**
 * A fragment to display a list of transactions
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class TransactionsFragment extends Fragment{

    private OnListFragmentInteractionListener mListener = new OnListFragmentInteractionListener();
    private MyTransactionRecyclerViewAdapter mAdapter;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TransactionsFragment() {
    }

//    // TODO: Customize parameter initialization
//    @SuppressWarnings("unused")
//    public static TransactionsFragment newInstance(int columnCount) {
//        TransactionsFragment fragment = new TransactionsFragment();
//        Bundle args = new Bundle();
//        args.putInt(ARG_COLUMN_COUNT, columnCount);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (getArguments() != null) {
//            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
//        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Transactions");
        View view = inflater.inflate(R.layout.fragment_transaction_list, container, false);
        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            mAdapter = new MyTransactionRecyclerViewAdapter(TransactionContent.ITEMS, mListener);
            recyclerView.setAdapter(mAdapter);
        }

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAdapter.notifyDataSetChanged();
        FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        fab.setImageResource(R.drawable.ic_add_24dp);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), CreateTransactionActivity.class);
                startActivity(intent);
            }
        });
    }

    public class OnListFragmentInteractionListener {
        public void onListFragmentInteraction(Transaction item, TextView title, TextView description, TextView category, TextView amount, TextView date) {

            TransactionDetailsFragment detailsFragment = new TransactionDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("transaction", item);

            //If it is lollipop (which supports shared elements), do the code necessary for transition
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                //Pass the transition names to the new details fragment
                bundle.putString("trans_title", title.getTransitionName());
                bundle.putString("trans_description", description.getTransitionName());
                bundle.putString("trans_category", category.getTransitionName());
                bundle.putString("trans_amount", amount.getTransitionName());
                bundle.putString("trans_date", date.getTransitionName());
                detailsFragment.setArguments(bundle);

                //Set the transitions
                setSharedElementReturnTransition(TransitionInflater.from(getActivity()).inflateTransition(R.transition.change_transaction_transition));
                setExitTransition(TransitionInflater.from(getActivity()).inflateTransition(android.R.transition.no_transition));
                detailsFragment.setSharedElementEnterTransition(
                        TransitionInflater.from(getActivity()).inflateTransition(R.transition.change_transaction_transition));
                detailsFragment.setEnterTransition(TransitionInflater.from(getActivity()).inflateTransition(android.R.transition.explode));

                //Create the transaction details fragment with the shared elements
                final FragmentTransaction ft = getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, detailsFragment)
                        .addToBackStack(null)
                        .addSharedElement(title, title.getTransitionName())
                        .addSharedElement(description, description.getTransitionName())
                        .addSharedElement(category, category.getTransitionName())
                        .addSharedElement(amount, amount.getTransitionName())
                        .addSharedElement(date, date.getTransitionName());

                //Wait some time before commiting the creation for the ripple animation to play
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ft.commit();
                    }
                }, 200);
            } else { //if not lollipop
                detailsFragment.setArguments(bundle);
                getFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, detailsFragment, MainActivity.FRAGMENT_TRANSACTIONDETAILS).addToBackStack(null).commit();
            }
        }
    }

    /**
     * Class to provide divider items between the list items.
     * Horizontal straight gray lines are added below each item.
     */
    public class DividerItemDecoration extends RecyclerView.ItemDecoration {

        private final int[] ATTRS = new int[]{android.R.attr.listDivider};

        private Drawable mDivider;

        /**
         * Default divider will be used
         */
        public DividerItemDecoration(Context context) {
            final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
            mDivider = styledAttributes.getDrawable(0);
            styledAttributes.recycle();
        }

        /**
         * Custom divider will be used
         */
        public DividerItemDecoration(Context context, int resId) {
            mDivider = ContextCompat.getDrawable(context, resId);
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }
}
